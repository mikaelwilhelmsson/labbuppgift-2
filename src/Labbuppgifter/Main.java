package Labbuppgifter;

public class Main {

    public static void main(String[] args) {

        CarFactory carFactory = new CarFactory();

        Car car1 = carFactory.createCar("BMW");
        Car car2 = carFactory.createCar("Volvo");
        Car car3 = carFactory.createCar("Ferarri");

        car1.drive();
        car2.drive();
        car3.drive();
    }
}


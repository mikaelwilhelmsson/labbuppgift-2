package Labbuppgifter;

public class CarFactory {

public Car createCar (String carType) {

        if (carType == null) {
        return null;

        } else if (carType.equalsIgnoreCase("BMW")) {
        return new BMW();

        } else if (carType.equalsIgnoreCase("Volvo")) {
        return new Volvo();

        } else if (carType.equalsIgnoreCase("Ferarri")) {
        return new Ferarri();
        }
        return null;
        }
        }
